#!/usr/bin/env python
# coding: utf-8
from __future__ import print_function
from getpass import getpass
import lastpass 
import sys

class LastPassClient(object):

    def __init__(self, username=None, password=None):
        if username is None:
            username = raw_input('LastPass Username: ')
        if password is None:
            password = getpass('LastPass Password: ')
        self.vault = lastpass.Vault.open_remote(username, password) 

   
    def search_accounts(self, search_string):
        return [acc for acc in self.vault.accounts 
                    if search_string in acc.name
                    or search_string in acc.url]

if __name__ == '__main__':
    username = password = search = None
    if len(sys.argv) > 3:
        search = sys.argv.pop()
    if len(sys.argv) > 1:
        password = sys.argv.pop()
        username = sys.argv.pop()

    c = LastPassClient(username, password)
    if not search:
        search = raw_input('Search: ')

    while search != '':
        accounts = c.search_accounts(search)
        if not accounts:
            print("Nothing found matching %s" % search)
        for acc in accounts:
            print(acc.name, acc.username, acc.password, acc.url)

        search = raw_input('Search: ')


