# Setup:

- Do the virtualenv thing unless you want to manage paths yourself.
- You'll need the python-dev package to compile crypto-related packages.  e.g.:
```sudo apt-get install python2.7-dev``` and likely others.
- ```pip install requirements.txt``` or install lastpass-python yourself
- Note that LastPass.com may detect the first usage as a new machine and
require verification via email.  What the email doesn't say is that you must
go to the verification URL from the same IP you tried to log in with.
If you're running this on a remote machine, such as a VPS, try opening the url
with lynx or maybe even curl or wget.

# Usage:

```./search_lastpass.py``` to be prompted for username/password/search.

Alternately, specify these on the commandline.  I strongly suggest prepending a
space to avoid putting your lastpass password in your shell history.

``` ./search_lastpass.py user@domain.com my_lastpass_password my_search_string```

The client will continue prompting for searches until you hit enter or ^C.

# Better usage:

Since activating the virtualenv for a single password lookup sucks, add
this to your .bash_profile or .bashrc:

```alias lp='/path/to/virtualenv/bin/python /path/to/client/search_lastpass.py```

And then when you open a new shell, you can just do this:

```lp``` or ``` lp username password search```

# Credits
This is all just a thin usage wrapper around
https://github.com/konomae/lastpass-python which in turn is a port of 
https://github.com/detunized/lastpass-ruby so thanks go to those devs.